<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "crawler".
 *
 * Auto generated 02-12-2022 10:15
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Site Crawler',
  'description' => 'Libraries and scripts for crawling the TYPO3 page tree.',
  'category' => 'module',
  'version' => '11.0.7',
  'state' => 'stable',
  'uploadfolder' => false,
  'clearcacheonload' => false,
  'author' => 'Tomas Norre Mikkelsen',
  'author_email' => 'tomasnorre@gmail.com',
  'author_company' => '',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '10.4.11-11.5.99',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
);

