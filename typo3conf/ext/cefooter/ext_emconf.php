<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "cefooter".
 *
 * Auto generated 02-12-2022 16:36
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Content element footer info',
  'description' => 'Preview of settings in footer of tt_content element in page module.',
  'category' => 'be',
  'version' => '1.4.0',
  'state' => 'stable',
  'uploadfolder' => false,
  'clearcacheonload' => true,
  'author' => 'Sven Burkert',
  'author_email' => 'bedienung@sbtheke.de',
  'author_company' => 'SBTheke web development',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '10.4.0-11.5.99',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
);

