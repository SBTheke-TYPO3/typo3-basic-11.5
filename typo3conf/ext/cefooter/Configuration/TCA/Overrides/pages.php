<?php
defined('TYPO3_MODE') || die('Access denied.');

// Register Page TSconfig
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    'cefooter',
    'Configuration/TypoScript/PageTS/mod.typoscript',
    'EXT:cefooter: Preview settings'
);