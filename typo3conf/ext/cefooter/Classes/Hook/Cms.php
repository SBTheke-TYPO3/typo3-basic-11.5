<?php
namespace SBTheke\Cefooter\Hook;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Backend\View\PageLayoutView;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2020 Sven Burkert <bedienung@sbtheke.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

class Cms implements \TYPO3\CMS\Backend\View\PageLayoutViewDrawFooterHookInterface
{

    /**
     * Preprocesses the preview footer rendering of a content element.
     *
     * @param PageLayoutView $parentObject Calling parent object
     * @param array $info Processed values
     * @param array $row Record row of tt_content
     */
    public function preProcess(PageLayoutView &$parentObject, &$info, array &$row) {
        $pageTSconfig = \TYPO3\CMS\Backend\Utility\BackendUtility::getPagesTSconfig($row['pid']);

        if($pageTSconfig['mod.']['web_layout.']['tt_content_footer.']['hideDefault']) {
            $info = [];
        }

        $showFields = $pageTSconfig['mod.']['web_layout.']['tt_content_footer.']['show'];
        if($showFields) {
            $showFields = GeneralUtility::trimExplode(',', $showFields);
            foreach($showFields as $k => $field) {
                $showFieldCond = $pageTSconfig['mod.']['web_layout.']['tt_content_footer.']['displayCond.'][$field . '.'];
                if(is_array($showFieldCond)) {
                    foreach($showFieldCond as $cond) {
                        $condField = $field;
                        if($cond['field']) {
                            $condField = $cond['field'];
                        }
                        if($cond['valuesNot']) {
                            if(in_array($row[$condField],
                                GeneralUtility::trimExplode(',', $cond['valuesNot']))) {
                                unset($showFields[$k]);
                            }
                        }
                        if($cond['values']) {
                            if(!in_array($row[$condField],
                                GeneralUtility::trimExplode(',', $cond['values']))) {
                                unset($showFields[$k]);
                            }
                        }
                    }
                }
            }
            if($showFields) {
                $parentObject->getProcessedValue('tt_content', implode(',', $showFields), $row, $info);
            }
        }
    }

}
