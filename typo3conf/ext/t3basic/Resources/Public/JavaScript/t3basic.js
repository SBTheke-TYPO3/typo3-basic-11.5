/**
 * JavaScript default file [TASK-javascript-4]
 */
$(function() {

    // Forms: Show/hide password in clear text [FEATURE-form-2]
    $('#show-hide-password a').on('click', function(e){
        e.preventDefault();
        $input = $('#show-hide-password input');
        if($input.attr('type') === 'text') {
            $input.attr('type', 'password');
            $('#show-hide-password .show-hide-password-close').show();
            $('#show-hide-password .show-hide-password-open').hide();
        } else if($input.attr('type') === 'password'){
            $input.attr('type', 'text');
            $('#show-hide-password .show-hide-password-open').show();
            $('#show-hide-password .show-hide-password-close').hide();
        }
    });

});
