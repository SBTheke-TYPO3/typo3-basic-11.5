<?php
defined('TYPO3') or die('Access denied.');

(function($table) { // Wrap code in function [TASK-be-2]

    // Show records in list module [TASK-tca-8]
    $GLOBALS['TCA'][$table]['ctrl']['hideTable'] = false;

})('sys_file');
