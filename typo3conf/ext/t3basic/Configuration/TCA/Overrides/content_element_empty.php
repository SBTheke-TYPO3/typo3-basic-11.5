<?php
defined('TYPO3_MODE') or die();

// Empty content element (to avoid language fallback for translations) [FEATURE-contentelement-1]

(function ($table) { // Wrap code in function [TASK-be-2]

    /***************
     * Add Content Element
     */
    if (!is_array($GLOBALS['TCA'][$table]['types']['empty'])) {
        $GLOBALS['TCA'][$table]['types']['empty'] = [];
    }

    /***************
     * Add content element to selector list
     */
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        $table,
        'CType',
        [
            'LLL:EXT:t3basic/Resources/Private/Language/backend.xlf:content_element.empty',
            'empty',
            'content-special-div'
        ],
        'html',
        'after'
    );

})('tt_content');
