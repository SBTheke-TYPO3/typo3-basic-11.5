<?php
defined('TYPO3') or die('Access denied.');

// Ensure that TCA overrides only apply if extension is loaded [TASK-tca-3]
if(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('bootstrap_package')) {

    (function($table) { // Wrap code in function [TASK-be-2]

        // Show records in list module and in search results in BE [TASK-tca-8]
        $GLOBALS['TCA'][$table]['ctrl']['hideTable'] = 0;
        $GLOBALS['TCA'][$table]['ctrl']['searchFields'] = 'header,bodytext';

    })('tx_bootstrappackage_tab_item');

}
