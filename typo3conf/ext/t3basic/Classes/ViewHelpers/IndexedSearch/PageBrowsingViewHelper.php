<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2020 Sven Burkert <bedienung@sbtheke.de>, SBTheke web development
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SBTheke\T3basic\ViewHelpers\IndexedSearch;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * EXT:indexed_search
 * Enhancement of EXT:indexed_search ViewHelper class for better HTML (CSS classes, jQuery)
 * and compatibility to Bootstrap and ARIA support [TASK-search-2]
 */
class PageBrowsingViewHelper extends \TYPO3\CMS\IndexedSearch\ViewHelpers\PageBrowsingViewHelper
{
    /**
     * @var string
     */
    protected static $prefixId = '#tx_indexedsearch';

    /**
     * @inheritDoc
     */
    public function render()
    {
        $maximumNumberOfResultPages = $this->arguments['maximumNumberOfResultPages'];
        $numberOfResults = $this->arguments['numberOfResults'];
        $resultsPerPage = $this->arguments['resultsPerPage'];
        $currentPage = $this->arguments['currentPage'];
        $freeIndexUid = $this->arguments['freeIndexUid'];

        if ($resultsPerPage <= 0) {
            $resultsPerPage = 10;
        }
        $pageCount = (int)ceil($numberOfResults / $resultsPerPage);
        // only show the result browser if more than one page is needed
        if ($pageCount === 1) {
            return '';
        }

        // Check if $currentPage is in range
        $currentPage = MathUtility::forceIntegerInRange($currentPage, 0, $pageCount - 1);

        $content = '';
        // prev page
        // show on all pages
        $label = LocalizationUtility::translate('displayResults.previous', 'IndexedSearch') ?? '';
        $content .= '<li class="page-item previous' . ($currentPage == 0 ? ' disabled' : '') . '">' . $this->makecurrentPageSelector_link($label, $currentPage - 1, $freeIndexUid, $currentPage == 0) . '</li>';
        // Check if $maximumNumberOfResultPages is in range
        $maximumNumberOfResultPages = MathUtility::forceIntegerInRange($maximumNumberOfResultPages, 1, $pageCount, 10);
        // Assume $currentPage is in the middle and calculate the index limits of the result page listing
        $minPage = $currentPage - (int)floor($maximumNumberOfResultPages / 2);
        $maxPage = $minPage + $maximumNumberOfResultPages - 1;
        // Check if the indexes are within the page limits
        if ($minPage < 0) {
            $maxPage -= $minPage;
            $minPage = 0;
        } elseif ($maxPage >= $pageCount) {
            $minPage -= $maxPage - $pageCount + 1;
            $maxPage = $pageCount - 1;
        }
        $pageLabel = LocalizationUtility::translate('displayResults.page', 'IndexedSearch');
        for ($a = $minPage; $a <= $maxPage; $a++) {
            $label = trim($pageLabel . ' ' . ($a + 1));
            $label = self::makecurrentPageSelector_link($label, $a, $freeIndexUid, false, $a === $currentPage);
            if ($a === $currentPage) {
                $content .= '<li class="page-item active">' . $label . '</li>';
            } else {
                $content .= '<li class="page-item">' . $label . '</li>';
            }
        }
        // next link
        $label = LocalizationUtility::translate('displayResults.next', 'IndexedSearch') ?? '';
        $content .= '<li class="page-item next' . ($currentPage == $pageCount - 1 ? ' disabled' : '') . '">' . self::makecurrentPageSelector_link($label, $currentPage + 1, $freeIndexUid) . '</li>';

        if (!$this->tag->hasAttribute('class')) {
            $this->tag->addAttribute('class', 'pagination');
        }
        $this->tag->setContent($content);
        return $this->tag->render();
    }

    /**
     * Used to make the link for the result-browser.
     * Notice how the links must resubmit the form after setting the new currentPage-value in a hidden formfield.
     *
     * @param string $str String to wrap in <a> tag
     * @param int $p currentPage value
     * @param string $freeIndexUid List of integers pointing to free indexing configurations to search. -1 represents no filtering, 0 represents TYPO3 pages only, any number above zero is a uid of an indexing configuration!
     * @param boolean $ariaDisabled Set attribute aria-disabled=true
     * @param boolean $ariaCurrent Set attribute aria-current=page
     * @return string Input string wrapped in <a> tag with onclick event attribute set.
     */
    protected function makecurrentPageSelector_link($str, $p, $freeIndexUid, $ariaDisabled = false, $ariaCurrent = false)
    {
        $onclick = '$(' . GeneralUtility::quoteJSvalue(self::$prefixId . '_pointer') . ').val(' . GeneralUtility::quoteJSvalue((string)$p) . ');';
        if ($freeIndexUid !== null) {
            $onclick .= '$(' . GeneralUtility::quoteJSvalue(self::$prefixId . '_freeIndexUid') . ').val(' . GeneralUtility::quoteJSvalue($freeIndexUid) . ');';
        }
        //document.getElementById('tx_indexedsearch_pointer').value='0';document.getElementById('tx_indexedsearch').submit();return false;
        $onclick .= '$(' . GeneralUtility::quoteJSvalue(self::$prefixId) . ').submit();return false;';
        return '<a class="page-link" href="#" onclick="' . htmlspecialchars($onclick) . '"' . ($ariaDisabled ? ' aria-disabled="true"' : '') . ($ariaCurrent ? ' aria-current="page"' : '') . '>' . htmlspecialchars($str) . '</a>';
    }
}
