<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2022 Sven Burkert <bedienung@sbtheke.de>, SBTheke web development
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SBTheke\T3basic\ViewHelpers;

use Closure;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Exception;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

/**
 * ViewHelper for parsing CSS classes in a usable format.
 * Needed for multiple selection of field "frame_class" [TASK-tca-5]
 *
 * @package SBTheke\T3basic\ViewHelpers
 */
class ClassSelectViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    /**
     * Initialize arguments.
     *
     * @throws Exception
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('classes', 'string', 'CSS classes to parse');
        $this->registerArgument('prefix', 'string', 'Prefix for CSS class');
        $this->registerArgument('prefixExclusion', 'string', 'CSS classes with no prefix');
    }

    /**
     * @param array $arguments
     * @param Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     * @return string
     */
    public static function renderStatic(
        array $arguments,
        Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ): string {
        if (!$arguments['classes']) {
            $arguments['classes'] = $renderChildrenClosure();
        }
        if ($arguments['classes']) {
            $classes = GeneralUtility::trimExplode(',', $arguments['classes']);
            if($arguments['prefix']) {
                $prefixExclusion = GeneralUtility::trimExplode(',', $arguments['prefixExclusion']);
                foreach($classes as &$class) {
                    if(!in_array($class, $prefixExclusion)) {
                        $class = $arguments['prefix'] . $class;
                    }
                }
            }
            return implode(' ', $classes);
        }
        return '';
    }
}
