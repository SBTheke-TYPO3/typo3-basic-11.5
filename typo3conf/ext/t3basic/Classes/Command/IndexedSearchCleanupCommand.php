<?php
namespace SBTheke\T3basic\Command;

/****************************************************************
 * Copyright notice
 *
 * (c) 2021 Sven Burkert <bedienung@sbtheke.de>, SBTheke web development
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Truncate indexed_search tables for reindexing [FEATURE-search-3]
 */
class IndexedSearchCleanupCommand extends Command
{
    /**
     * Configure the command
     */
    protected function configure(): void
    {
        $this->setDescription('Truncate tables of EXT:indexed_search for reindexing');
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int error code
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);

        $connectionPool->getConnectionForTable('index_debug')
            ->truncate('index_debug');
        $connectionPool->getConnectionForTable('index_fulltext')
            ->truncate('index_fulltext');
        $connectionPool->getConnectionForTable('index_grlist')
            ->truncate('index_grlist');
        $connectionPool->getConnectionForTable('index_phash')
            ->truncate('index_phash');
        $connectionPool->getConnectionForTable('index_rel')
            ->truncate('index_rel');
        $connectionPool->getConnectionForTable('index_section')
            ->truncate('index_section');
        $connectionPool->getConnectionForTable('index_words')
            ->truncate('index_words');

        return 0;
    }
}
