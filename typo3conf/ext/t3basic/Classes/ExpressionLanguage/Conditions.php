<?php
namespace SBTheke\T3basic\ExpressionLanguage;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2023 Sven Burkert <bedienung@sbtheke.de>, SBTheke web development
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Exception;
use SBTheke\T3basic\Utility\DatabaseUtility;

/**
 * Check if content type or plugin is anywhere on this page [FEATURE-typoscript-1]
 * Example 1: [t3basic.isElementOnCurrentPageInCurrentLanguage('image')]
 * Example 2: [t3basic.isElementOnCurrentPageInCurrentLanguage('news_pi1')]
 * Example 3: [t3basic.isElementOnCurrentPageInCurrentLanguage('container_2_columns')]
 * @todo Enable content elements implemented with type "shortcut" (reference)
 * @testing page /tests/typoscript-conditions/ (ID 215)
 * @see EXT:powermail (https://github.com/einpraegsam/powermail/blob/develop/Classes/Condition/PowermailConditionFunctionsProvider.php function "isPluginExistingOnCurrentPageInCurrentLanguage")
 */
class Conditions
{

    /**
     * @param string $conditionParameter e.g. "image" or "news_pi1"
     * @return bool
     * @throws DBALException
     * @throws Exception
     */
    public function isElementOnCurrentPageInCurrentLanguage(string $conditionParameter): bool
    {
        if (TYPO3_MODE === 'FE') {
            $queryBuilder = DatabaseUtility::getQueryBuilderForTable('tt_content');
            $found = $queryBuilder
                ->select('uid')
                ->from('tt_content')
                ->where(sprintf(
                    'pid=%d AND sys_language_uid=%d AND (list_type="%3$s" OR CType="%3$s")',
                    $GLOBALS['TSFE']->id,
                    $GLOBALS['TSFE']->getLanguage()->getLanguageId(),
                    $conditionParameter
                ))
                ->setMaxResults(1)
                ->execute()
                ->fetchOne();
            return !empty($found);
        }
        return false;
    }

}
