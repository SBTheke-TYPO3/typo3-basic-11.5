<?php
declare(strict_types = 1);

namespace SBTheke\T3basic\Icons;

use BK2K\BootstrapPackage\Icons\IconList;
use BK2K\BootstrapPackage\Icons\IconProviderInterface;
use BK2K\BootstrapPackage\Icons\SvgIcon;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class FontawesomeSolidProvider implements IconProviderInterface
{
    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return 'EXT:t3basic/Resources/Public/Icons/FontAwesome/solid/';
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'FontAwesome Solid';
    }

    /**
     * @param string $identifier
     * @return bool
     */
    public function supports(string $identifier): bool
    {
        return 'EXT:t3basic/Resources/Public/Icons/FontAwesome/solid/' === $identifier;
    }

    /**
     * @return IconList
     */
    public function getIconList(): IconList
    {
        $icons = new IconList();

        $directory = 'EXT:t3basic/Resources/Public/Icons/FontAwesome/solid/';
        $path = GeneralUtility::getFileAbsFileName($directory);
        $files = iterator_to_array(new \FilesystemIterator($path, \FilesystemIterator::KEY_AS_PATHNAME));
        ksort($files);

        foreach ($files as $key => $fileinfo) {
            if ($fileinfo instanceof \SplFileInfo
                && $fileinfo->isFile()
                && strtolower($fileinfo->getExtension()) === 'svg'
            ) {
                $icons->addIcon(
                    (new SvgIcon())
                        ->setSrc($directory . $fileinfo->getFilename())
                        ->setIdentifier($directory . $fileinfo->getFilename())
                        ->setName($fileinfo->getBasename('.' . $fileinfo->getExtension()))
                        ->setPreviewImage($directory . $fileinfo->getFilename())
                );
            }
        }

        return $icons;
    }
}
