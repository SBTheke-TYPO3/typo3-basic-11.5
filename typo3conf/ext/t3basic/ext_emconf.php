<?php
/***************************************************************
 * Extension Manager/Repository config file for ext "t3basic".
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'T3Basic for mypage',
    'description' => 'Elementary configuration of TYPO3 system and individual configuration for mypage.',
    'category' => 'misc',
    'version' => '9.0.0',
    'state' => 'stable',
    'uploadfolder' => false,
    'createDirs' => '',
    'clearcacheonload' => true,
    'author' => 'Sven Burkert',
    'author_email' => 'bedienung@sbtheke.de',
    'author_company' => 'SBTheke web development',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-11.5.99',
            'bootstrap_package' => '12.0.5-12.99.99',
            'vhs' => '',
        ],
        'conflicts' => [],
        'suggests' => [
            'form' => '',
            'indexed_search' => '',
            'container' => '',
            'news' => '',
            'cefooter' => '',
            'cs_seo' => '',
            'default_upload_folder' => '',
            'image_autoresize' => '',
        ],
    ],
];
