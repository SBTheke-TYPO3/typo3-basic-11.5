<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "default_upload_folder".
 *
 * Auto generated 15-10-2023 12:11
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Default upload folder',
  'description' => 'Make it possible to configure the default upload folder for a certain TCA column',
  'category' => 'be',
  'version' => '1.3.0',
  'state' => 'stable',
  'uploadfolder' => false,
  'clearcacheonload' => false,
  'author' => 'Frans Saris',
  'author_email' => 't3ext@beecht.it',
  'author_company' => 'Beech.it',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '10.4.0-11.5.99',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
);

