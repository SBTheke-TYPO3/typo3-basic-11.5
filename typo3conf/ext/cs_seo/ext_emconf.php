<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "cs_seo".
 *
 * Auto generated 15-10-2023 12:11
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => '[clickstorm] SEO',
  'description' => 'SEO Extension: Enables important onpage features for search engine optimization (SEO). Expands
						the page settings and any desired records for example with a preview for Google search results (SERP)
						Structured Data (JSON-LD) and a Focus Keyword. Restrictive hreflang and canonical tags. Modules for
						metadata of records and alternative texts of images. Further features are shown in the extension manual.',
  'category' => 'services',
  'version' => '7.3.2',
  'state' => 'stable',
  'uploadfolder' => false,
  'clearcacheonload' => true,
  'author' => 'Pascale Beier, Angela Dudtkowski, Marc Hirdes, Andreas Kirilow, Alexander König - clickstorm GmbH',
  'author_email' => 'hirdes@clickstorm.de',
  'author_company' => 'clickstorm GmbH',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '11.2.0-11.5.99',
      'seo' => '11.2.0-11.5.99',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
);

