<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "mbox".
 *
 * Auto generated 30-01-2024 18:53
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'mbox Mail Client',
  'description' => 'TYPO3 CMS backend module to view mbox file contents, like an email client.',
  'category' => 'backend',
  'version' => '2.1.0',
  'state' => 'stable',
  'uploadfolder' => false,
  'clearcacheonload' => false,
  'author' => 'Armin Vieweg',
  'author_email' => 'armin@v.ieweg.de',
  'author_company' => 'IW Medien GmbH | www.iwmedien.de',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '11.5.0-12.9.99',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
  'autoload' => 
  array (
  ),
);

