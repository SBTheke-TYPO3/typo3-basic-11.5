<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "webp".
 *
 * Auto generated 15-10-2023 12:12
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Creates WebP copies for images',
  'description' => 'Creates WebP copies of all jpeg and png images',
  'category' => 'fe',
  'version' => '4.2.0',
  'state' => 'stable',
  'uploadfolder' => false,
  'clearcacheonload' => false,
  'author' => 'Wolfgang Klinger',
  'author_email' => 'wk@plan2.net',
  'author_company' => 'plan2net GmbH',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '10.4.0-11.5.99',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
);

