<?php
# PackageStates.php

# This file is maintained by TYPO3's package management. Although you can edit it
# manually, you should rather use the extension manager for maintaining packages.
# This file will be regenerated automatically if it doesn't exist. Deleting this file
# should, however, never become necessary if you use the package commands.

return [
    'packages' => [
        'core' => [
            'packagePath' => 'typo3/sysext/core/',
        ],
        'scheduler' => [
            'packagePath' => 'typo3/sysext/scheduler/',
        ],
        'extbase' => [
            'packagePath' => 'typo3/sysext/extbase/',
        ],
        'fluid' => [
            'packagePath' => 'typo3/sysext/fluid/',
        ],
        'frontend' => [
            'packagePath' => 'typo3/sysext/frontend/',
        ],
        'fluid_styled_content' => [
            'packagePath' => 'typo3/sysext/fluid_styled_content/',
        ],
        'filelist' => [
            'packagePath' => 'typo3/sysext/filelist/',
        ],
        'impexp' => [
            'packagePath' => 'typo3/sysext/impexp/',
        ],
        'form' => [
            'packagePath' => 'typo3/sysext/form/',
        ],
        'install' => [
            'packagePath' => 'typo3/sysext/install/',
        ],
        'info' => [
            'packagePath' => 'typo3/sysext/info/',
        ],
        'linkvalidator' => [
            'packagePath' => 'typo3/sysext/linkvalidator/',
        ],
        'reports' => [
            'packagePath' => 'typo3/sysext/reports/',
        ],
        'redirects' => [
            'packagePath' => 'typo3/sysext/redirects/',
        ],
        'recordlist' => [
            'packagePath' => 'typo3/sysext/recordlist/',
        ],
        'backend' => [
            'packagePath' => 'typo3/sysext/backend/',
        ],
        'indexed_search' => [
            'packagePath' => 'typo3/sysext/indexed_search/',
        ],
        'recycler' => [
            'packagePath' => 'typo3/sysext/recycler/',
        ],
        'setup' => [
            'packagePath' => 'typo3/sysext/setup/',
        ],
        'rte_ckeditor' => [
            'packagePath' => 'typo3/sysext/rte_ckeditor/',
        ],
        'adminpanel' => [
            'packagePath' => 'typo3/sysext/adminpanel/',
        ],
        'belog' => [
            'packagePath' => 'typo3/sysext/belog/',
        ],
        'beuser' => [
            'packagePath' => 'typo3/sysext/beuser/',
        ],
        'extensionmanager' => [
            'packagePath' => 'typo3/sysext/extensionmanager/',
        ],
        'felogin' => [
            'packagePath' => 'typo3/sysext/felogin/',
        ],
        'filemetadata' => [
            'packagePath' => 'typo3/sysext/filemetadata/',
        ],
        'lowlevel' => [
            'packagePath' => 'typo3/sysext/lowlevel/',
        ],
        'seo' => [
            'packagePath' => 'typo3/sysext/seo/',
        ],
        'sys_note' => [
            'packagePath' => 'typo3/sysext/sys_note/',
        ],
        't3editor' => [
            'packagePath' => 'typo3/sysext/t3editor/',
        ],
        'tstemplate' => [
            'packagePath' => 'typo3/sysext/tstemplate/',
        ],
        'bootstrap_package' => [
            'packagePath' => 'typo3conf/ext/bootstrap_package/',
        ],
        'cefooter' => [
            'packagePath' => 'typo3conf/ext/cefooter/',
        ],
        'container' => [
            'packagePath' => 'typo3conf/ext/container/',
        ],
        'cs_seo' => [
            'packagePath' => 'typo3conf/ext/cs_seo/',
        ],
        'default_upload_folder' => [
            'packagePath' => 'typo3conf/ext/default_upload_folder/',
        ],
        'image_autoresize' => [
            'packagePath' => 'typo3conf/ext/image_autoresize/',
        ],
        'news' => [
            'packagePath' => 'typo3conf/ext/news/',
        ],
        'vhs' => [
            'packagePath' => 'typo3conf/ext/vhs/',
        ],
        't3basic' => [
            'packagePath' => 'typo3conf/ext/t3basic/',
        ],
        'crawler' => [
            'packagePath' => 'typo3conf/ext/crawler/',
        ],
        'mbox' => [
            'packagePath' => 'typo3conf/ext/mbox/',
        ],
        'webp' => [
            'packagePath' => 'typo3conf/ext/webp/',
        ],
    ],
    'version' => 5,
];
