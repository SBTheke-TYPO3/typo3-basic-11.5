<?php
return array (
  'aliasToClassNameMapping' => 
  array (
    'tx_crawler_api' => 'AOE\\Crawler\\Api\\CrawlerApi',
    'tx_crawler_domain_events_dispatcher' => 'AOE\\Crawler\\Event\\EventDispatcher',
    'tx_crawler_domain_events_observer' => 'AOE\\Crawler\\Event\\EventObserverInterface',
    'tx_crawler_domain_process' => 'AOE\\Crawler\\Domain\\Model\\Process',
    'tx_crawler_domain_process_collection' => 'AOE\\Crawler\\Domain\\Model\\ProcessCollection',
    'tx_crawler_domain_process_manager' => 'AOE\\Crawler\\Service\\ProcessService',
    'tx_crawler_domain_process_repository' => 'AOE\\Crawler\\Domain\\Repository\\ProcessRepository',
    'tx_crawler_domain_queue_entry' => 'AOE\\Crawler\\Domain\\Model\\Queue',
    'tx_crawler_domain_queue_repository' => 'AOE\\Crawler\\Domain\\Repository\\QueueRepository',
    'tx_crawler_domain_reason' => 'AOE\\Crawler\\Domain\\Model\\Reason',
    'tx_crawler_hooks_processcleanup' => 'AOE\\Crawler\\Hooks\\ProcessCleanUpHook',
    'tx_crawler_hooks_tsfe' => 'AOE\\Crawler\\Hooks\\TsfeHook',
    'tx_crawler_lib' => 'AOE\\Crawler\\Controller\\CrawlerController',
    'tx_crawler_modfunc1' => 'AOE\\Crawler\\Backend\\BackendModule',
    'tx_crawler_tcafunc' => 'AOE\\Crawler\\Utility\\TcaUtility',
  ),
  'classNameToAliasMapping' => 
  array (
    'AOE\\Crawler\\Api\\CrawlerApi' => 
    array (
      'tx_crawler_api' => 'tx_crawler_api',
    ),
    'AOE\\Crawler\\Event\\EventDispatcher' => 
    array (
      'tx_crawler_domain_events_dispatcher' => 'tx_crawler_domain_events_dispatcher',
    ),
    'AOE\\Crawler\\Event\\EventObserverInterface' => 
    array (
      'tx_crawler_domain_events_observer' => 'tx_crawler_domain_events_observer',
    ),
    'AOE\\Crawler\\Domain\\Model\\Process' => 
    array (
      'tx_crawler_domain_process' => 'tx_crawler_domain_process',
    ),
    'AOE\\Crawler\\Domain\\Model\\ProcessCollection' => 
    array (
      'tx_crawler_domain_process_collection' => 'tx_crawler_domain_process_collection',
    ),
    'AOE\\Crawler\\Service\\ProcessService' => 
    array (
      'tx_crawler_domain_process_manager' => 'tx_crawler_domain_process_manager',
    ),
    'AOE\\Crawler\\Domain\\Repository\\ProcessRepository' => 
    array (
      'tx_crawler_domain_process_repository' => 'tx_crawler_domain_process_repository',
    ),
    'AOE\\Crawler\\Domain\\Model\\Queue' => 
    array (
      'tx_crawler_domain_queue_entry' => 'tx_crawler_domain_queue_entry',
    ),
    'AOE\\Crawler\\Domain\\Repository\\QueueRepository' => 
    array (
      'tx_crawler_domain_queue_repository' => 'tx_crawler_domain_queue_repository',
    ),
    'AOE\\Crawler\\Domain\\Model\\Reason' => 
    array (
      'tx_crawler_domain_reason' => 'tx_crawler_domain_reason',
    ),
    'AOE\\Crawler\\Hooks\\ProcessCleanUpHook' => 
    array (
      'tx_crawler_hooks_processcleanup' => 'tx_crawler_hooks_processcleanup',
    ),
    'AOE\\Crawler\\Hooks\\TsfeHook' => 
    array (
      'tx_crawler_hooks_tsfe' => 'tx_crawler_hooks_tsfe',
    ),
    'AOE\\Crawler\\Controller\\CrawlerController' => 
    array (
      'tx_crawler_lib' => 'tx_crawler_lib',
    ),
    'AOE\\Crawler\\Backend\\BackendModule' => 
    array (
      'tx_crawler_modfunc1' => 'tx_crawler_modfunc1',
    ),
    'AOE\\Crawler\\Utility\\TcaUtility' => 
    array (
      'tx_crawler_tcafunc' => 'tx_crawler_tcafunc',
    ),
  ),
);
